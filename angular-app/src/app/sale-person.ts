export class SalePerson {

    constructor(public firstname:string,
                public lastname:string,
                public email:string,
                public saleVolum:number){
                }
}
