import { Component, OnInit } from '@angular/core';
import { SalePerson } from '../sale-person';

@Component({
  selector: 'app-sales-person-list',
  templateUrl: './sales-person-list.component.html',
  styleUrls: ['./sales-person-list.component.css']
})
export class SalesPersonListComponent implements OnInit {

  salesPersonList: SalePerson[]=[

    new SalePerson('momodou','sene','momodou@ut.edu.sn',20000),
    new SalePerson('moussa','bakhoum','moussa@bakhoum.sn',4000),
    new SalePerson('El mbaye', 'sene', 'elmbyae@gmail.com',6000),
    new SalePerson('amina','soumbounou','amina@gmail.com',2000)
  ]
  constructor() { }


  ngOnInit(): void {
  }

}
